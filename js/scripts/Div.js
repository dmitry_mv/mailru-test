(function (window) {
    'use strict';

    var $ = window.jQuery;

    var defaultOptions = {
        cssSels: {}
    };

    // получаем конструктор вьюшки кнопки
    var Div = window.ru.mail.cpf.Basic.getView({
        _Handlers: {
            dom: {}
        },
        _Init: function () {
            var self = this;
            $(window.ru.mail.cpf.modules.Dropdown).on("myCustomEvent", function(e, data){
                self._elems.textContainer.text(data.innerText);
            });
        }

    }, defaultOptions, null, 'Div');

    // Публикуем ссылку на конструктор
    window.getNameSpace('ru.mail.cpf.modules').Div = Div;

})(window);
