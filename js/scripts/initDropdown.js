(function (window) {
    'use strict';

    var dropdownParams = {
        cssSels: {
            Main: {
                textContainer: '.text-container'
            }
        }
    };

    window.ru.mail.cpf.modules.Dropdown(dropdownParams, null, $('#wrapper-container'));

})(window);
