(function (window) {
    'use strict';

    var dropdownParams = {
        cssSels: {
            Main: {
                textContainer: '.text-container'
            }
        }
    };

    window.ru.mail.cpf.modules.Div(dropdownParams, null, $('#div-container'));

})(window);
