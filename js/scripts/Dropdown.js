(function (window) {
    'use strict';

    var $ = window.jQuery;

    var defaultOptions = {
        cssSels: {
            wrapper: '.wrapper-container',
            dropdown_wrapper: '.dd',
            menu: '.dd__menu',
            dropdown: '.dd__ul',
            item: '.dd__ul__li'
        },
        states: {
            activeDropdown: 'expanded',
            activeMenuItem: 'active'
        }
    };

    // получаем конструктор вьюшки кнопки
    var Dropdown = window.ru.mail.cpf.Basic.getView({
        _Handlers: {
            dom: {
                'click:wrapper': function () {
                    this.dropdownHide();
                },
                'click:menu': function (e) {
                    e.stopPropagation();
                    this.dropdownToggle(e.target);
                },
                'click:item': function (e) {
                    e.stopPropagation();
                    this.consoleLog(e.target.innerText);
                    this.dropdownSelect(e.target);
                    this.dropdownHide();
                }
            }
        },
        _Init: function () {

        },

        dropdownSelect: function (item) {
            $(item).parent('.dd__ul').find('.dd__ul__li').removeClass(this._opts.states.activeMenuItem);
            $(item).addClass(this._opts.states.activeMenuItem);
            $(window.ru.mail.cpf.modules.Dropdown).trigger("myCustomEvent", [item]);
        },
        //hide for all dropdowns
        dropdownHide: function () {
            $(this._opts.cssSels.dropdown_wrapper).removeClass(this._opts.states.activeDropdown);
        },
        dropdownToggle: function (target) {
            $(target).parent().toggleClass(this._opts.states.activeDropdown);
        },
        consoleLog: function (text) {
            console.log(text);
        },
        //TODO: немного не понял, что именно хочется, чтобы он возвращал
        getState: function () {
            return {
              options: this._opts
            };
        }
    }, defaultOptions, null, 'Dropdown');

    // Публикуем ссылку на конструктор
    window.getNameSpace('ru.mail.cpf.modules').Dropdown = Dropdown;

})(window);
